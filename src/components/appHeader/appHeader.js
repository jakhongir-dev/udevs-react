import React from "react";
import "./appHeader.css";

export default function AppHeader() {
  return (
    <div className="app-header">
      <h1 className="title">ToDo List</h1>
      <p className="process">3 more to do, 0 done</p>
    </div>
  );
}
