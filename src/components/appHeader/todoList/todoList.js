import React from "react";
import "./todoList.css";
import { RiDeleteBinLine } from "react-icons/ri";
import { FaExclamation } from "react-icons/fa";

export default function TodoList({ todos }) {
  return (
    <ul className="todo-list">
      {todos.map((item, index) => (
        <li key={index} className="list-item">
          {item.label}{" "}
          <div className="icons">
            <RiDeleteBinLine className="btn-delete" />{" "}
            <FaExclamation className="btn-highlight" />
          </div>
        </li>
      ))}
    </ul>
  );
}
