import logo from "./logo.svg";
import "./App.css";
import AppHeader from "./components/appHeader/appHeader";
import { useState } from "react";
import TodoList from "./components/appHeader/todoList/todoList";
import SearchPanel from "./components/SearchPanel/SearchPanel";
import FilterPanel from "./components/FilterPanel/FilterPanel";
import TodoAddForm from "./components/TodoAdd/TodoAdd";

import { RiDeleteBinLine } from "react-icons/ri";
import { FaExclamation } from "react-icons/fa";

function App() {
  const [todos, setTodos] = useState([
    {
      label: "Drink Coffee",
      important: false,
      done: false,
      id: 1,
    },
    {
      label: "Make Awesome App",
      important: false,
      done: false,
      id: 2,
    },
    {
      label: "Have a lunch",
      important: false,
      done: false,
      id: 3,
    },
  ]);
  return (
    <div className="App">
      <AppHeader></AppHeader>
      <div className="search">
        <SearchPanel></SearchPanel>
        <FilterPanel></FilterPanel>
      </div>
      <TodoList todos={todos}></TodoList>
      <TodoAddForm></TodoAddForm>
    </div>
  );
}

export default App;
