import React from "react";
import "./TodoAdd.css";

export default function TodoAddForm() {
  return (
    <div className="todo-add">
      <input className="add-input" placeholder="what needs to be done"></input>
      <button className="btn-add">Add Item</button>
    </div>
  );
}
