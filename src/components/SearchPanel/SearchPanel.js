import React from "react";
import "./SearchPanel.css";

export default function SearchPanel() {
  return (
    <div className="search-panel">
      <input className="search-input" placeholder="type to search"></input>
    </div>
  );
}
