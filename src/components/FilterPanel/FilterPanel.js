import React from "react";
import "./FilterPanel.css";

export default function FilterPanel() {
  return (
    <div className="filter-panel">
      <button className="btn btn-all">All</button>
      <button className="btn btn-active">Active</button>
      <button className="btn btn-done">Done</button>
    </div>
  );
}
